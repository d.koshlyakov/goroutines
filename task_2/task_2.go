package main

import (
	"context"
	"time"
	"fmt"
)

// Задача №2
// 1. Задачка из разряда собеса, на контекст
// Дано:
// Функция foo на вход принимает контекст, возвращает int
// Внутри вызывает другую функцию bar, которая не
// принимает контекст, тоже возвращает int
// Foo возвращает то, что вернула bar
//
// Необходимо:
// Если контекст отменят раньше, чем отработает bar,
// вернуть 0

// 1. работа с каналами
// 2. select{}
// 3. как завершать обе горутины через контекст

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	fmt.Println(foo(ctx))
}

func foo(ctx context.Context) int {
	ch := make(chan int)
	go func() {
		ch <- bar()
	}()
	for {
		select {
		case <-ctx.Done():
			return 0
		case t := <-ch:
			return t
		}
	}
}

func bar() int {
	time.Sleep(time.Second * 3)
	return 4
}
