package main

import "fmt"

// Задача №4
// Можно ли объединить несколько каналов в один?

func main() {
	ca, cb := buildChan(5, 1), buildChan(5, 2)
	out := make(chan int)

	mergeChan(out, ca, cb)

	for v := range out {
		fmt.Printf("%v ", v)
	}
}

func mergeChan(out chan<- int, ca, cb <-chan int) {
	go func() {
		defer close(out)
		doneA, doneB := false, false
		for !doneA || !doneB {
			select {
			case a, ok := <-ca:
				if !ok {
					doneA = true
					continue
				}
				out <- a
			case b, ok := <-cb:
				if !ok {
					doneB = true
					continue
				}
				out <- b
			}
		}
	}()
}

func buildChan(count int, val int) <-chan int {
	c := make(chan int)
	go func() {
		for i := 0; i < count; i++ {
			c <- val
		}
		close(c)
	}()
	return c
}
