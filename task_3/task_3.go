package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

// Задача №3
// 1. Гонка за данными
// 2. Конкуретно писать в мапу или другую глобальную переменную
// 3. здесь наглядно показать как использовать пакеты sync.Mutex, пакет sync.Map, пакет atomic

func main() {
	example1()
	//example2()
	//example3()
	//example4()
}

func example1() {
	i := 1
	mu := sync.Mutex{}

	go func() {
		mu.Lock()
		i++
		mu.Unlock()
	}()

	go func() {
		mu.Lock()
		i++
		mu.Unlock()
	}()

	time.Sleep(time.Second * 1)

	fmt.Println(i)
}

func example2() {
	var i int64
	go func() {
		atomic.AddInt64(&i, 1) //Атомарное увеличение значения i
	}()

	go func() {
		atomic.AddInt64(&i, 1) //То же самое
	}()

	// Пакет sync/atomic предоставляет примитивы для int32,
	// int64, uint32 и uint64, но не для int. Вот почему в этом примере
	// i имеет тип int64.
	fmt.Println(i)
}

func example3() {
	// использовать пакет sync.Mutex
	myMap := make(map[string]int, 2)

	wg := sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()
		myMap["total"] = myMap["total"] + 5 //запись в мапу
	}()

	go func() {
		defer wg.Done()
		myMap["total"] = myMap["total"] + 15 //запись в мапу
	}()

	wg.Wait()

	fmt.Println(myMap)
}

func example4() {
	myMap := sync.Map{}
	myMap.Store("total", 0)

	wg := sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()

		total, _ := myMap.Load("total")
		myMap.Store("total", total.(int)+5)
	}()

	go func() {
		defer wg.Done()

		total, _ := myMap.Load("total")
		myMap.Store("total", total.(int)+15)
	}()

	wg.Wait()

	total, ok := myMap.Load("total")
	if ok {
		fmt.Println(total)
	}
}
