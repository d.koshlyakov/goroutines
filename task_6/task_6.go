package main

import (
	"fmt"
	"sync"
)

// Задача №6
// 1. Написать сортировку в которой использование горутин приводит
// не к оптимизации, а наоборот, к ухудшению
// 2. результаты показать бенчкарками

func main() {
	var b = []int{35, 9, 8, 6, 20, 7, 4, 3, 2, 1, 89, 45, 67, 4, 11, 16, 90}
	//fmt.Println(quickSort(b))
	fmt.Println(mergeSort(b))
}

func quickSort(arr []int) []int {
	if len(arr) <= 1 {
		return nil
	}
	pivot := arr[0]
	left, right := 1, len(arr)-1

	for left <= right {
		for left <= right && arr[left] < pivot {
			left++
		}
		for left <= right && arr[right] > pivot {
			right--
		}
		if left <= right {
			arr[left], arr[right] = arr[right], arr[left]
			left++
			right--
		}
	}
	arr[0], arr[right] = arr[right], arr[0]
	quickSort(arr[:right])
	quickSort(arr[right+1:])

	return arr
}

func mergeSort(c []int) []int {
	var n = len(c)
	if n < 2 {
		return c
	}
	var m = n / 2
	var lCol = c[:m]
	var lSize = len(lCol)
	if lSize >= 2 {
		lCol = mergeSort(lCol)
	}

	var rCol = c[m:]
	var rSize = len(rCol)
	if rSize >= 2 {
		rCol = mergeSort(rCol)
	}
	var tmp = make([]int, 0)
	var i, j = 0, 0
	for i < lSize && j < rSize {
		if lCol[i] < rCol[j] {
			tmp = append(tmp, lCol[i])
			i++
		} else {
			tmp = append(tmp, rCol[j])
			j++
		}

	}
	if i < lSize {
		tmp = append(tmp, lCol[i:]...)
	}
	if j < rSize {
		tmp = append(tmp, rCol[j:]...)
	}
	return tmp
}
func mergeSortParallel(c []int) []int {
	var n = len(c)
	if n < 2 {
		return c
	}
	var m = n / 2
	var lCol = c[:m]
	var lSize = len(lCol)
	if lSize >= 2 {
		lCol = mergeSort(lCol)
	}

	var rCol = c[m:]
	var rSize = len(rCol)
	if rSize >= 2 {
		rCol = mergeSort(rCol)
	}
	var tmp = make([]int, 0)
	var i, j = 0, 0
	for i < lSize && j < rSize {
		if lCol[i] < rCol[j] {
			tmp = append(tmp, lCol[i])
			i++
		} else {
			tmp = append(tmp, rCol[j])
			j++
		}

	}
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		if i < lSize {
			tmp = append(tmp, lCol[i:]...)
		}
	}()

	go func() {
		defer wg.Done()
		if j < rSize {
			tmp = append(tmp, rCol[j:]...)
		}
	}()

	wg.Wait()
	return tmp
}
