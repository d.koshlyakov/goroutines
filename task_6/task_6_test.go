package main

import (
	"testing"
)

func BenchmarkMergeSort(b *testing.B) {
	var arr = []int{35, 9, 8, 6, 20, 7, 4, 3, 2, 1, 89, 45, 67, 4, 11, 16, 90, 87, 44, 40, 99, 101}

	for i := 0; i < b.N; i++ {
		mergeSort(arr)
	}
}

func BenchmarkMergeSortParallel(b *testing.B) {
	var arr = []int{35, 9, 8, 6, 20, 7, 4, 3, 2, 1, 89, 45, 67, 4, 11, 16, 90, 87, 44, 40, 99, 101}

	for i := 0; i < b.N; i++ {
		mergeSortParallel(arr)
	}
}

func BenchmarkQuickSort(b *testing.B) {
	var arr = []int{35, 9, 8, 6, 20, 7, 4, 3, 2, 1, 89, 45, 67, 4, 11, 16, 90, 87, 44, 40, 99, 101}

	for i := 0; i < b.N; i++ {
		quickSort(arr)
	}
}
