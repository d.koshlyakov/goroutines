package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

// Задача №7
// необходимо реализовать long pooling
// не большой сервис который будет ожидать сообщения
// или по таймеру ходить в БД за новыми сообщениями например

// в задаче умышленно допустил ошибки, будем думать как исправить

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	pool := NewPoolMsg()
	go pool.worker()

	pool.start(ctx)
}

type PoolMsg struct {
	chanDone chan struct{}
	chanMsg  chan string
	wg       sync.WaitGroup
}

func NewPoolMsg() *PoolMsg {
	return &PoolMsg{
		chanDone: make(chan struct{}),
		chanMsg:  make(chan string),
		wg:       sync.WaitGroup{},
	}
}

func (p *PoolMsg) start(ctx context.Context) {
	p.wg.Add(1)
	go func() {
		defer p.wg.Done()
		ticker := time.NewTicker(2 * time.Second)
		defer ticker.Stop()

		for {
			select {
			case <-ctx.Done():
				fmt.Println("worker stopped done")
				return
			case <-ticker.C:
				p.getMessages()
			}
		}
	}()

	p.wg.Wait()
}

func (p *PoolMsg) worker() {
	for msg := range p.chanMsg {
		fmt.Println(msg)
	}
}

func (p *PoolMsg) getMessages() {
	messages := []string{
		"msg_1",
		"msg_2",
		"msg_3",
		"msg_4",
		"msg_5",
	}

	for _, msg := range messages {
		go func(msg string) {
			select {
			case <-p.chanDone:
				return
			default:
				p.chanMsg <- msg
			}
		}(msg)
	}
}
