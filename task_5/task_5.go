package main

import (
	"fmt"
	"time"
)

// Задача №5
// 1. поиграть в пинг-понг
// 2. здесь наглядно показать как использовать каналы
// 3. чтение, запись, закрыть канал
// При использовании канала Go применимый принцип:
// Не закрывайте канал с принимающей стороны и не закрывайте
// канал с несколькими одновременными отправителями.

func main() {
	ch := make(chan string)

	go play(ch, "ping")
	go play(ch, "pong")

	ch <- "start game"

	time.Sleep(time.Second * 5)

}

// про направление каналов
func play(ch chan string, hit string) {
	for {
		select {
		case res, ok := <-ch:
			fmt.Println(res)
			time.Sleep(time.Second * 1)
			if ok {
				ch <- hit
			}
		}
	}
}
